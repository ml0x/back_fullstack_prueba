package com.Prueba.dbController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONObject;

public class MySqlController {
	static String jdbcUrl;
	static String username;
	static String password;
	static String driver;

	public static Connection conn = null;

	public MySqlController(String jdbcUrl, String username, String password, String driver) {
		try {
			if (driver != null)
				Class.forName(driver);
			conn = DriverManager.getConnection(jdbcUrl, username, password);
		} catch (Exception e) {
			System.out.println("Error Conexion: " + e.getMessage());
		}
	}

	public JSONObject sqlCommand(String sql, String command) {
		JSONObject res = new JSONObject();
		res.put("error", 1);
		res.put("message", "Instruccion no ejecutada");
		try {
			JSONArray dataArr = new JSONArray();
			Statement stmt = (Statement) conn.createStatement();

			if (command == "execute") {
				stmt.execute(sql);
			} else {
				ResultSet rs = stmt.executeQuery(sql);
				ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
				int columnCount = rsmd.getColumnCount();

				while (rs.next()) {
					JSONObject row = new JSONObject();
					for (int i = 1; i <= columnCount; i++) {
						String key = rsmd.getColumnName(i);
						row.put(key, rs.getString(key));
					}
					dataArr.put(row);
				}
			}
			res.put("data", dataArr);
			res.put("error", 0);
			res.put("message", "OK");
			stmt.close();
		} catch (SQLException e) {
			res.put("error", 1);
			res.put("message", e.getMessage());
			res.put("printStackTrace", "" + e.getStackTrace()[1].getLineNumber() + "");
			res.put("e", "SQLException");
		} catch (Exception e) {
			res.put("error", "1");
			res.put("message", e.getMessage());
			res.put("printStackTrace", "" + e.getStackTrace()[1].getLineNumber() + "");
			res.put("e", "Exception");
			res.put("sql", sql);
		}
		return res;
	}
	
	public void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			System.out.println("Error cerrar conexion: " + e.getMessage());
		}
	}
}
