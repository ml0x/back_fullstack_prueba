package com.Prueba.main.service;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.Prueba.dbController.MySqlController;

@RestController
public class Service {
	@Autowired
	HttpServletRequest request;
	
	@RequestMapping(value = "/test", method = { RequestMethod.GET }, produces = "application/json")
	@CrossOrigin(origins = "*", methods = { RequestMethod.OPTIONS, RequestMethod.GET })
	public String test(@RequestParam String s) throws JSONException, Exception {
		JSONObject j = new JSONObject();
		j.put("message", s);
		return j.toString();
	}
	
	@RequestMapping(value = "/getEncuesta", method = { RequestMethod.GET }, produces = "application/json")
	@CrossOrigin(origins = "*", methods = { RequestMethod.OPTIONS, RequestMethod.GET })
	public Object getEncuesta() throws JSONException, Exception {
		JSONObject salida = new JSONObject();
		MySqlController db;
			db = new MySqlController(
					"jdbc:mysql://127.0.0.1/prueba?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root",
					"",
					"com.mysql.jdbc.Driver"
			);
		String queryConsulta = "select * from prueba.encuesta;";
		try {
			JSONObject datos = db.sqlCommand(queryConsulta, "");
			JSONArray recibidos = datos.getJSONArray("data");
			salida.put("error", 0);
			salida.put("data", recibidos);
			return salida.toMap();
		} catch (Exception e) {
			salida.put("error", 1);
			salida.put("message", e.getMessage());
			e.printStackTrace();
			return salida.toMap();
		}
	}
	
	@RequestMapping(value = "/addEncuesta", method = { RequestMethod.GET }, produces = "application/json")
	@CrossOrigin(origins = "*", methods = { RequestMethod.OPTIONS, RequestMethod.GET })
	public Object addEncuesta() throws JSONException, Exception {
		JSONObject salida = new JSONObject();
		MySqlController db;
			db = new MySqlController(
					"jdbc:mysql://127.0.0.1/prueba?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root",
					"",
					"com.mysql.jdbc.Driver"
			);
		String queryConsulta = "select * from prueba.encuesta;";
		try {
			JSONObject datos = db.sqlCommand(queryConsulta, "");
			JSONArray recibidos = datos.getJSONArray("data");
			salida.put("error", 0);
			salida.put("data", recibidos);
			return salida.toMap();
		} catch (Exception e) {
			salida.put("error", 1);
			salida.put("message", e.getMessage());
			e.printStackTrace();
			return salida.toMap();
		}
	}
	
	@RequestMapping(value = "/addRegistro", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(origins = "*", methods = { RequestMethod.OPTIONS, RequestMethod.POST, RequestMethod.GET })
	@ResponseStatus(value = HttpStatus.OK)
	public Object addRegistro(@RequestBody String json) {
		
		JSONObject salida = new JSONObject();
		JSONObject input = new JSONObject(json);
		
		MySqlController db;
			db = new MySqlController(
					"jdbc:mysql://127.0.0.1/prueba?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					"root",
					"",
					"com.mysql.jdbc.Driver"
			);
		String queryConsulta = "insert into prueba.encuesta (id, correo, estiloMusical) values (null, '"+input.getString("correo")+"', '"+input.getString("estiloMusical")+"');";
		System.out.println(queryConsulta);
		try {
			db.sqlCommand(queryConsulta, "execute");
			salida.put("message", "Registro agregado correctamente");
			return salida.toMap();
		} catch (Exception e) {
			salida.put("error", 1);
			salida.put("message", e.getMessage());
			e.printStackTrace();
			return salida.toMap();
		}
	}
	
}
